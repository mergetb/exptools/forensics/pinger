module gitlab.com/mergetb/exptools/forensics/pinger

go 1.16

require (
	github.com/go-ping/ping v1.1.0
	gitlab.com/mergetb/exptools/forensics/measurement v0.0.0-20221117181041-206dd9c061ce
	gopkg.in/yaml.v3 v3.0.1
)
