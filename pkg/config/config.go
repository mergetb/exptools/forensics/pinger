package config

import (
	"fmt"
	"log"
	"os"
	"time"

	"gopkg.in/yaml.v3"
)

const (
	dflt_influx_server   string = "http://localhost:8086"
	dflt_influx_database string = "forensics"
)

type Config struct {
	Host           string            `yaml:"host"`
	Count          int               `yaml:"count"`
	Interval       time.Duration               `yaml:"interval"`
	Size           int               `yaml:"size"`
	Ttl            int               `yaml:"ttl"`
	Tags           map[string]string `yaml:"tags"`
	InfluxServer   string            `yaml:"influx_server"`
	InfluxDatabase string            `yaml:"influx_database"`
}

func GetConfigFromYaml(filename string) (*Config, error) {
	data, err := os.ReadFile(filename)
	if err != nil {
		return nil, fmt.Errorf("read %s: %v", filename, err)
	}

	cfg := new(Config)
	err = yaml.Unmarshal(data, cfg)
	if err != nil {
		return nil, fmt.Errorf("unmarshal %s: %v", filename, err)
	}

	if cfg.InfluxServer == "" {
		log.Printf("config does not define influx_server; defaulting to %s", dflt_influx_server)
		cfg.InfluxServer = dflt_influx_server
	}

	if cfg.InfluxDatabase == "" {
		log.Printf("config does not define influx_database; defaulting to %s", dflt_influx_database)
		cfg.InfluxDatabase = dflt_influx_database
	}

	if cfg.Host == "" {
		return nil, fmt.Errorf("config must define Host")
	}

	return cfg, nil
}
